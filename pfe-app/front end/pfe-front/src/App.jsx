import "./App.css";
import Navbar from "./nav/Navbar";
function App() {
  return (
    <>
      <Navbar />
      <div className="bg-blue-600 w-full h-screen"></div>;
    </>
  );
}

export default App;
